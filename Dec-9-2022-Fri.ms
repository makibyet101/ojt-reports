.TL
End of the Day Report
.AU
Mark Nhel A. Besonia
.AI
Innovuze Solutions, Inc
.DA

.QP
.IP \(bu 2
Explored further on
.I "logrotate"
and configured it to rotate into a different directory.
.IP \(bu 2
Explored
.I "minikube",
a tool that makes it easy to run Kubernetes locally.
