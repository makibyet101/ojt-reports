.TL
End of the Day Report
.AU
Mark Nhel A. Besonia
.AI
Innovuze Solutions, Inc
.DA

.QP
.IP \(bu 2
Configured
.I "rsyslog"'s
logrotation with compression and date extension.
.IP \(bu 2
Successfully made a script for the auto renewal of a SSL certificate in
.I "letsencrypt"
using
.I "certbot"'s
.I "standalone"
option.
