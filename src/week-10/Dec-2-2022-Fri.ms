.TL
End of the Day Report
.AU
Mark Nhel A. Besonia
.AI
Innovuze Solutions, Inc
.DA

.QP
.IP \(bu 2
Installed and configured
.I "prometheus"
with
.I "prometheus-node-exporter"
as an exporter and
.I "grafana"
as a graphical frontend using lxc.
.IP \(bu 2
Provided assistance at the scouting of relocation site of the company.
