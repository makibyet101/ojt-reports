.TL
End of the Day Report
.AU
Mark Nhel A. Besonia
.AI
Innovuze Solutions, Inc
.DA

.QP
.IP \(bu 2
Studied
.I "supervisor",
.I "rsyslog",
and
.I "nginx"
in linode instances.
.IP \(bu 2
Explored
.I "nginx"
permanent redirect (301) to redirect web requests using IP to its domain name.
